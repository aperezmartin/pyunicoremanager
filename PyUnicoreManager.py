#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
    PyUnicoreManager (It adapts PyUnicore library and uses it from different frameworks)
    Author: Aarón Pérez Martín
    Contact:a.perez.martin@fz-juelich.de
    Organization: Forschungszentrum Jülich

    PyUnicore library (Client for UNICORE using the REST API)
    For full info of the REST API, see https://sourceforge.net/p/unicore/wiki/REST_API/
'''
#

import pyunicore.client as unicore_client
from base64 import b64encode
import os, time, sys, logging, datetime
from enum import IntEnum

def set_logger(name, level='INFO'):
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.propagate = False

    # Configure stream handler for the cells
    handler = logging.StreamHandler(stream=sys.stdout)
    handler.setFormatter(logging.Formatter('%(asctime)s [%(levelname)s] %(name)s - %(message)s', '%H:%M:%S'))
    handler.setLevel(level)

    logger.addHandler(handler)
    return logging.root.manager.loggerDict[name]


logss = set_logger("ndp.pyunim")


class Method(IntEnum):
    # Different access require a different token and the way to call a job
    ACCESS_JUDOOR = 0
    ACCESS_COLLAB = 1


class Authentication():
    def __init__(self, token: str, access: Method, server: str, **args):
        self.token = token
        self.access = access
        self.server = server


class Utils():
    @staticmethod
    def execution_time(start, end, num_dec):
        total_time = end - start
        if total_time < 60:
            msg = "Flow time --- " + str(round(total_time, num_dec)) + " seconds ---"
        elif 60 <= total_time < 3600:
            msg = "Flow time --- " + str(round(total_time / 60, num_dec)) + " minutes ---"
        elif 3600 <= total_time < 86400:
            msg = "Flow time --- " + str(round(total_time / 3600, num_dec)) + " hours ---"
        else:
            msg = "Flow time --- " + str(round(total_time / 86400, num_dec)) + " days ---"
        return msg

    @staticmethod
    def generateToken(judooraccount, juddorpassword):
        return b64encode(str.encode(judooraccount + ":" + juddorpassword)).decode("ascii")

    @staticmethod
    def arrayToString(array):
        return ' '.join(map(str, array))

    @staticmethod
    def generate_PythonCompiler(variables):
        return [" cd " + variables["destination_project_path"],
                "date >> text; echo done!"]

    @staticmethod
    def generate_steps_bashscript(variables, filename):
        return ["cd " + variables["destination_project_path"],
                'log=' + variables["destination_log_path"] + '/log$(date "+%Y%m%d")',
                "chmod 764 " + filename + " >>$log ",
                "./ " + filename + ' >>$log']


class Environment_UNICORE():

    def __init__(self, auth: Authentication, **args):
        self.urls, self.conn_info, self.job_info, self.script_info = {}, {}, {}, {}

        # Required for a sever connection
        self.conn_info["token"] = auth.token
        self.conn_info["methodToAccess"] = auth.access
        self.conn_info["serverToConnect"] = auth.server
        self.conn_info["serverProjectName"] = args.get('serverProjectName')
        self.conn_info["serverToRegister"] = args.get('serverToRegister',
                                                      "https://zam2125.zam.kfa-juelich.de:9112/HBP/rest/registries/default_registry")

        self.urls["destination_server_endpoint"] = args.get('serverProjectName', "PROJECT")


class PyUnicoreManager(object):
    def __init__(self, environment, **args):

        destination_server_endpoint = "PROJECT"

        self.storage = None
        self.env = environment

        self.verbose = args.get('verbose', False)
        self.check_environment = args.get('check_environment', False)
        self.clean_job_storages = args.get('clean_job_storages', False)
        has_errors = False

        if self.env.conn_info["token"] is None:
            logss.error("Token is required!")
            has_errors = True

        # Accesing with JUDOOR or COLLAB token have have different parameters in the PyUnicore.Transport
        # oidc=False doesnt work with collab token
        self.transport = None
        if Method.ACCESS_JUDOOR == self.env.conn_info["methodToAccess"]:
            self.transport = unicore_client.Transport(self.env.conn_info["token"], oidc=False)
        elif Method.ACCESS_COLLAB == self.env.conn_info["methodToAccess"]:
            self.transport = unicore_client.Transport(self.env.conn_info["token"])

        # if self.env.conn_info["serverProjectName"]:
        #    self.transport.preferences = "group:" + str(self.env.conn_info["serverProjectName"])

        self.registry = unicore_client.Registry(self.transport, self.env.conn_info["serverToRegister"])
        self.site = self.registry.site(self.env.conn_info["serverToConnect"])
        self.client = unicore_client.Client(self.transport, self.site.site_url)

        try:
            for storage in self.site.get_storages():
                if storage.storage_url.endswith(self.env.urls["destination_server_endpoint"]):
                    self.storage = storage
                    break
        except Exception as e:
            logss.error("Source not available " + self.env.conn_info["serverProjectName"])
            return

        if not self.storage:
            logss.error("Source not available " + self.env.urls["destination_server_endpoint"])
            has_errors = True

        if self.clean_job_storages:
            self.clean_storages(endswith="-uspace")

        # Get the object Storage
        # Endpoint of Storage is mapped from env variables of your account into the UNICORE
        if self.env.conn_info["serverProjectName"]:
            # we need to access to the right project folder into the HPC system
            # First, setting an environment variable by a job
            if self.check_environment:
                logss.info("Creating environment variables into UNICORE system")
                result = self.one_run(
                    steps=["jutil env activate -p " + str(self.env.conn_info["serverProjectName"]),
                           "echo $PROJECT"])

                if len(result["stderr"]) > 0:
                    logss.error("Error " + result["stderr"])
                    has_errors = True
                else:
                    logss.info(" - Variable $PROJECT is now " + result["stdout"])

        if has_errors:
            logss.error("\nPlease provide the right parameters.")

    def getStorage(self):
        if (self.verbose):
            for storage in self.client.get_storages():
                logss.info(storage.storage_url)

        return self.client.get_storages()

    def getJobs(self, status=None):
        result = []
        list = self.client.get_jobs()
        if status:
            for j in list:
                if j.properties['status'].lower() == status.lower():
                    result.append(j)
            return result
        else:
            return list

    def getSites(self):
        return unicore_client.get_sites(self.transport)

    def clean_storages(self, endswith):
        count = 0
        if len(self.client.get_storages()) >= 200 and len(endswith) > 0:
            logss.info("Cleaning storage endpoints. Working on it...")
            for storage in self.client.get_storages():
                msg = ""
                if storage.storage_url.endswith(str(endswith)):
                    self.transport.delete(url=storage.storage_url)
                    count += 1
                    msg = str(storage.storage_url).split("/")[-1] + " has been removed"
                else:
                    msg = "Keep: " + str(storage.storage_url)

                if self.verbose:
                    logss.info(msg)
            logss.info("Storage endpoints removed: " + str(count))
        else:
            logss.info("Storage endpoints are still under limit: " + str(len(self.client.get_storages())))

    def createJob(self, list_of_steps, job_args=None):

        executable = ""
        for item in list_of_steps[:-1]:
            executable += item + " \n "
        executable += list_of_steps[-1]  # Last element can not contain the new line symbol

        if (self.verbose):
            logss.info("Executing commands...")
            for item in list_of_steps:
                logss.info(" -" + str(item))

        job = dict(job_args) if job_args else dict()
        job['Executable'] = executable

        return job

    def __run_job(self, job):
        result_job = {}
        try:
            cmd_job = self.client.new_job(job_description=job)
            # logss.info(cmd_job.properties['status'])

            # print(datetime.datetime.now().strftime("[%H:%M:%S]"),"Job status...", cmd_job.properties['status'])
            logss.info("Job status... " + str(cmd_job.properties['status']))

            # Wait until the job finishes
            while (cmd_job.properties['status'] == "QUEUED"):
                time.sleep(0.25)
            # print(datetime.datetime.now().strftime("[%H:%M:%S]"),cmd_job.properties['status'])
            logss.info(cmd_job.properties['status'])
            cmd_job.poll()

            wd = cmd_job.working_dir
            result_job["stderr"] = ''.join([x.decode('utf8') for x in wd.stat("/stderr").raw().readlines()])
            result_job["stdout"] = ''.join([x.decode('utf8') for x in wd.stat("/stdout").raw().readlines()])
            if self.verbose:
                logss.info(result_job.items())
            if (cmd_job.properties['status'] == "FAILED"):
                raise Exception(str(result_job["stderr"]))                
            else:
                logss.info('Job finished!')

        except Exception as e:
            logss.error(str(e))
            return None, None
        return cmd_job, result_job

    def uploadFiles(self, filesToUpload):
        try:
            if len(filesToUpload) == 0:
                logss.info("Nothing to upload")
                return

            # Uploading files
            logss.info("Uploading to " + str(self.storage.storage_url))
            list_files = list()
            # destination="collab/filename"
            for file_info in filesToUpload:
                filename = str(file_info[0]).split('/')[-1]
                self.storage.upload(str(file_info[0]), destination=os.path.join(
                    file_info[1]))  # it works like this ../PROJECT/ "collab/filename"
                if (self.verbose):
                    logss.info(" - Uploaded file" + filename)  # getting the last element
                list_files.append(filename)
            logss.info("Uploaded all files: "+str(list_files))
            return True
        except Exception as e:
            logss.error("Uploading error", e)
            return False

    def downloadFiles(self, filesToDownload):
        try:
            if len(filesToDownload) == 0:
                logss.info("Nothing to download")
                return

            logss.info("Downloading from " + self.storage.storage_url)
            list_files = list()
            for file_info in filesToDownload:
                filename = str(file_info[1]).split('/')[-1]  # getting the last element
                remote = self.storage.stat(file_info[1])  # internal links works like this ../PROJECT/ "collab/filename"
                remote.download(os.path.join(file_info[0]))
                if (self.verbose):
                    logss.info(" - Downloaded file" + filename)
                list_files.append(filename)
            logss.info("Downloaded all files: "+str(list_files))
            return True
        except Exception as e:
            logss.error("Downloading error", e)
            return False

    def one_run(self, steps, parameters):
        if len(steps) == 0:
            logss.info("No instructions to execute")
            return

        # Executing a job
        if self.verbose:
            logss.info("STEPS: "+ str(steps))
            
        job = self.createJob(list_of_steps=steps, job_args=parameters)
        cmd_job, result_job = self.__run_job(job)
        return result_job


#################
"""
env = Environment_UNICORE(token=Utils.generateToken("user", "pass"),
                 access= Utils.ACCESS_JUDOOR,
                 serverToConnect="JUSUF",
                 serverProjectName="cslns",
                 serverArgs=,
                 destination_project_path="/p/project/cslns/perezmartin1/cbs",
                 destination_relative_subfolder="")
py = PyUnicoreManager(environment = env, clean_job_storages=False, verbose=False)
result = py.one_run(steps=["module load Python SciPy-Stack numba PyCUDA","export CUDA_VISIBLE_DEVICES=0 ","nvidia-smi"])
print(result)

"""
